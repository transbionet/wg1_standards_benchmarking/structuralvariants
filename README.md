# StructuralVariants

The **WG1 Standards & Benchmarking** has defined a **Structural Variant** benchmarking action based on the information gathered on the survey filled by TransBionet members.

This action involves the detection of CNVs on 90 WES from phase3 1000 genomes project.  As gold standard, we will use experimentally validated CNVs from three previous microarray studies [**McCarroll et al. 2008**](https://doi.org/10.1038/ng.238); [**Conrad et al. 2010**](https://doi.org/10.1038/nature08516); [**International HapMap C, 2010**](https://doi.org/10.1038/nature09298). Besides, reference CNVs will be categorized as common and rare based on their frequency rate across samples in order to assess the pipeline performance in both scenarios [**Jiang et al. 2018**](https://doi.org/10.1186/s13059-018-1578-y).

You can find four files available at [**1stChallenge**](./1stChallenge/) folder:

   1.  [**phase3_WES_fastq.txt**](./1stChallenge/phase3_WES_fastq.txt): urls to fastq files for direct download (link, SRRaccession, Sample ID...). Be aware that several fastq files can be associated to one unique sample.
   2.  [**exome_pull_down_targets.bed**](./1stChallenge/exome_pull_down_targets.bed): exome capture design BED file (hg19).
   3.  [**CNV_output_example.txt**](./1stChallenge/CNV_output_example.txt): Tabulated file with 5 mandatory and 2 optional fields.
   4.  [**1stChallenge.SV.Germline.pdf**](./1stChallenge/1stChallenge.SV.Germline.pdf): Brief presentation with benchmark strategy details.

Considering the above materials, the expected steps to produce your own results would be.

   1. Obtain all files listed in file [**phase3_WES_fastq.txt**](./1stChallenge/phase3_WES_fastq.txt). Be aware that several fastq files can be associated to one unique sample!.
   2.  Processed each sample following your CNV analysis pipeline.
   3.  Make sure your output format follows the convention specified in file [**CNV_output_example.txt**](./1stChallenge/CNV_output_example.txt):  
      `"chr   start   end   sample  cnv_type num_exons(optional)  copy_number(optional)"`
   4.  Submit your results into TransBioNet dedicated GitLab folder [**Results/**](./Results/) by creating a pull request.
   5. Don't forget to describe your analytical protocol by including it in the [**protocols/**](./protocols/) folder.

Some notes and considerations:
- CNV calling pipelines will be run using the hg19 genome assembly on downloaded FastQ files.

- Output files will help us setting up and optimise the OpenEBench platform to address the evaluation of methods using the selected gold standard.

- Protocols will  contribute to the development of bioinformatics clinical guidelines to be used by TransBioNet members and other groups in healthcare settings.

In case of any doubt, please, contact by email to Pablo Minguez and Lorena de la Fuente, chairs of the task, together with the INB Hub.

Have a look at the [**Relevant Literature References**](./RelevantLiterature.md) for further information on the articles cited here.
