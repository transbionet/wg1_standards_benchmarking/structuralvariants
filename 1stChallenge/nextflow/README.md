# CNV detection benchmarking workflow with Nextflow

**Structural Variant benchmarking** workflow used to assess the performance of variants predicted by TransBionet WG1 members.

This workflow structure is compatible with the OpenEBench [Virtual Research Environment (VRE)](https://openebench.bsc.es/submission/workspace/).

## Description

The workflow takes an input file with CNV predictions (i.e. the results provided by a participant), computes a set of metrics, and compares them against the data currently stored in OpenEBench within the TransBioNet community. Two assessment metrics are provided for that predicitions. Also, some plots (which are optional) that allow to visualize the performance of the tool are generated. The workflow consists in three standard steps, defined by OpenEBench. The tools needed to run these steps are in a single Docker image, generated from the code found in [this folder](../container). Separated instances are spawned from the image for each step:
1. **Validation**: the input file format is checked and, if required, the content of the file is validated (e.g check whether the submitted gene IDs exist)
2. **Metrics Computation**: the predictions are compared with the 'Gold Standards' provided by the community, which results in two performance metrics - precision (Positive Predictive Value) and recall(True Positive Rate).
3. **Results Consolidation**: the benchmark itself is performed by merging the tool metrics with the rest of the data. The results are provided in JSON format and SVG format (scatter plot).

## Usage
In order to use the workflow you need to:
* Install [Nextflow](https://www.nextflow.io/)
* Clone this in a separate directory, and build locally the Docker image found in [the 'container' folder](../container). Use the following command ```docker build -t transbionet_cnv:latest .```.
* Run it just using *`nextflow run main.nf`*. Arguments specifications:
```
	   	    Usage:
	    Run the pipeline with default parameters:
	    nextflow run main.nf -profile docker
	    Run with user parameters:
 	    nextflow run main.nf -profile docker --input {predictions.file} --public_ref_dir {validation.reference.file} --participant_id {tool.name} --goldstandard_dir {gold.standards.dir} --challenges_ids {analyzed.challenges} --assess_dir {benchmark.data.dir} --results_dir {output.dir}
	    Mandatory arguments:
                --input		List of CNVs predicted
				--community_id			Name or OEB permanent ID for the benchmarking community
                --public_ref_dir 		Directory with input fastq samples used to validate the predictions
                --participant_id  		Name of the tool used for prediction
                --goldstandard_dir 		Dir that contains metrics reference datasets (gold standards) for all challeneges
                --challenges_ids  		List of challenges selected by the user, separated by spaces
                --assess_dir			Dir where the data for the benchmark are stored
	    Other options:
                --validation_result		The output directory where the results from validation step will be saved
				--assessment_results	The output directory where the results from the computed metrics step will be saved
				--outdir	The output directory where the consolidation of the benchmark will be saved
				--statsdir	The output directory with nextflow statistics
				--data_model_export_path	The output path where json file with benchmarking data model contents will be saved
	  			--otherdir	
```

Default input parameters and Docker images to use for each step can be specified in the [config](./nextflow.config) file
**NOTE: In order to make your workflow compatible with the [OpenEBench VRE Nextflow Executor](https://github.com/inab/vre-process_nextflow-executor), please make sure to use the same parameter names in your workflow.**
