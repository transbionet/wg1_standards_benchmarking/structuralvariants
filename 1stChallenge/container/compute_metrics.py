#!/usr/bin/env python

########################################################

# Author: Lorena de la Fuente & Javier Garrayo Ventas
# Date: 09/09/2019

########################################################


import sys
sys.path.insert(1, '/home/proyectos/bioinfo/lodela/SVbenchmark/CODEX2/OpenEBench/scripts/')
from JSON_templates import JSON_templates
import io
import os
import pandas
import math
import json
import argparse
import subprocess
from collections import defaultdict




def main(args):

	
	# Assuring the output path does exist
	if not os.path.exists(os.path.dirname(args.output)):
		try:
			os.makedirs(os.path.dirname(args.output))
			with open(args.output, mode="a"):
				pass
		except OSError as exc:
			print("OS error: {0}".format(exc) + "\nCould not create output path: " + args.output)



	# Run benchmark

	subprocess.call(["/usr/bin/Rscript", "--vanilla", os.path.dirname(os.path.abspath(__file__)) +  "/CNVbenchmark.R", "-i", args.participant_data, "-p", args.participant_name, "-o", args.output.split("\.json")[0]+".tsv", "-g", args.gold_standard, "-t", args.targets])

	
	# open metrics file and create json assessment file for each methic
	# define array that will hold the full set of assessment datasets

	assessments = []
	std_error = 0

	with open(args.output+".tsv", "r") as metrics_file:

		for line in metrics_file:
			
			[participant, gold_standard, comparison_type, cnv_type, metric, metric_value] = line.split("\t")

			challenge = gold_standard + "_" + comparison_type + "_" + cnv_type
			data_id = args.community_name + ":" + challenge + "_" + participant + "_" + metric 
			
			assessments.append(JSON_templates.write_assessment_dataset(data_id, args.community_name, challenge, participant, metric, metric_value, std_error))



	# once all assessments have been added, print to json file

	with io.open(args.output,
			mode='w', encoding="utf-8") as f:
		jdata = json.dumps(assessments, sort_keys=True, indent=4, separators=(',', ': '))
		f.write(jdata)




if __name__ == "__main__":

	#arguments
	parser = argparse.ArgumentParser(description="WG1_standards_benchmarking")
	parser.add_argument("-i", "--participant_data", help="participant_data", required=True)
	parser.add_argument("-p", "--participant_name", help="institutional name", required=True)
	parser.add_argument("-c", "--community_name", help="name/id of benchmarking community", required=True)
	parser.add_argument("-o", "--output", help="output path where assessment JSON files will be written", required=True)
	parser.add_argument("-g", "--gold_standard", help="gold_standard_file", required=False, default="/home/proyectos/bioinfo/lodela/SVbenchmark/CODEX2/tosend/goldStandard.xlsx")
	parser.add_argument("-t", "--targets", help="targets_file", required=False, default="/home/proyectos/bioinfo/lodela/SVbenchmark/CODEX2/tosend/exome_pull_down_targets/20130108.exome.targets.bed")


	args = parser.parse_args()

	main(args)


