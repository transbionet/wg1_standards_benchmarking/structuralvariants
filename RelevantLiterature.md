# Relevant Literature for the effort.

---

## Benchmarking datasets.

- **A robust benchmark for detection of germline large deletions and insertions** | Zook, J. M. et al. A robust benchmark for detection of germline large deletions and insertions. Nat Biotechnol 38, 1347–1355 (2020). https://doi.org/10.1038/s41587-020-0538-8.

- **Comparative study of whole exome sequencing-based copy number variation detection tools** | Zhao, L., Liu, H., Yuan, X. et al. Comparative study of whole exome sequencing-based copy number variation detection tools. BMC Bioinformatics 21, 97 (2020). https://doi.org/10.1186/s12859-020-3421-1

- **CODEX2: full-spectrum copy number variation detection by high-throughput DNA sequencing** | Jiang, Y., Wang, R., Urrutia, E. et al. CODEX2: full-spectrum copy number variation detection by high-throughput DNA sequencing. Genome Biol 19, 202 (2018). https://doi.org/10.1186/s13059-018-1578-y

- **Integrating common and rare genetic variation in diverse human populations** | International HapMap C, Altshuler, D., Gibbs, R. et al. Integrating common and rare genetic variation in diverse human populations. Nature 467, 52–58 (2010). https://doi.org/10.1038/nature09298

- **Origins and functional impact of copy number variation in the human genome** | Conrad, D., Pinto, D., Redon, R. et al. Origins and functional impact of copy number variation in the human genome. Nature 464, 704–712 (2010). https://doi.org/10.1038/nature08516

- **Integrated detection and population-genetic analysis of SNPs and copy number variation** | McCarroll, S., Kuruvilla, F., Korn, J. et al. Integrated detection and population-genetic analysis of SNPs and copy number variation. Nat Genet 40, 1166–1174 (2008). https://doi.org/10.1038/ng.238

## Relevant articles regarding the use of genomics in the clinics.

- **Best practices for the analytical validation of clinical whole-genome sequencing intended for the diagnosis of germline disease** | Marshall, C.R., Chowdhury, S., Taft, R.J. et al. Best practices for the analytical validation of clinical whole-genome sequencing intended for the diagnosis of germline disease. npj Genom. Med. 5, 47 (2020). https://doi.org/10.1038/s41525-020-00154-9

- **Standards and Guidelines for Validating Next-Generation Sequencing Bioinformatics Pipelines: A Joint Recommendation of the Association for Molecular Pathology and the College of American Pathologists** | Roy, S., Coldren, C., Karunamurthy, A. et al. Standards and Guidelines for Validating Next-Generation Sequencing Bioinformatics Pipelines: A Joint Recommendation of the Association for Molecular Pathology and the College of American Pathologists. J Mol Diagn. 2018 Jan;20(1):4-27. https://doi.org/10.1016/j.jmoldx.2017.11.003
